echo "Updating bot..."
git config credential.helper store
git fetch --all
git reset --hard
git pull https://gitlab.com/Gsbhasin84/zap.git
echo "Updated bot"
echo "Killing old bot if running..."
pkill -f Zap-Bot
echo "Done"
echo "Starting bot..."
bash -c "exec -a Zap-Bot ruby bot.rb"

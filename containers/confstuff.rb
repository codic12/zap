require 'discordrb'
require 'confrb'
require 'shellwords'
module ConfStuff
  extend Discordrb::Commands::CommandContainer
  command(:modconf, description: "Modify configuration per-user") do |event, entry, value|
    if entry.downcase == "embed_color"
      $db.mknested event.author.id.to_s
      if File.exist?(File.join(".zap", event.author.id.to_s, "embed_color.cfg"))
        $db.mkcfg(File.join(event.author.id.to_s, "embed_color.cfg"), value) # Mkcfg overwrites, instead of appending.
      else
        $db.mkcfg(File.join(event.author.id.to_s, "embed_color.cfg"), value, newline: false)
      end
    end
    nil # This is so Discord doesn't return the exit code of the db operations!
  end
end

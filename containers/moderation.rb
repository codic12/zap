require 'discordrb'
module Moderation
    extend Discordrb::Commands::CommandContainer
    command(:purge, description: "Purge/prune {amount} messages", required_permissions: [:manage_messages]) do |event, amount|
        amount = amount.to_i + 1
        event.channel.prune(amount)
        msg = event.respond("Pruned #{amount-1} message(s). Note that messages older than 14 days cannot be deleted. Self destructing in 3 seconds.")
        sleep 3
        event.channel.delete_message msg
    end
    command(:purgeall, description: "Purge/prune all channel messages", required_permissions: [:manage_messages]) do |event|
        eval "event.channel.prune(100)\n" * 7 # Tries clearing 700 messages, using 7 calls since 100 is the max that can be pruned (API limit)
        msg = event.respond("Pruned 700 messages. Note that messages older than 14 days cannot be deleted. If you have messages left that are not older than 14 days, run again. Self destructing in 3 seconds.")
        sleep 3
        event.channel.delete_message msg
    end
    command(:kick, description: "Kick a user with specified reason (default reason: nil)", required_permissions: [:kick_members]) do |event, user, reason|
        u = event.bot.parse_mention(user)
        event.server.kick(u, reason)
        event.respond("Kicked #{user} for #{reason}.")
    end
    command(:ban, description: "Ban a user with specified reason (default reason: nil)", required_permissions: [:ban_members]) do |event, user, reason|
        u = event.bot.parse_mention(user)
        event.server.ban(u, reason: reason)
        event.respond("Banned #{user} for #{reason}.")
    end
    command(:unban, description: "Unban a user with specified reason (default reason: nil)", required_permissions: [:ban_members]) do |event, user, reason|
        u = event.bot.parse_mention(user)
        event.server.unban(u, reason: reason)
        event.respond("Unbanned #{user} for #{reason}.")
    end
end

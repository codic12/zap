require 'yaml'; require 'discordrb'
OIDs = YAML.load(File.read '.env')['OIDs']
module ZapHelp
    extend Discordrb::Commands::CommandContainer
    command(:help) do |event, cmd|
        if cmd == nil
            event.send_embed do |e|
                e.title = "Zap - Help"
                e.color = data_fetch(event, "embed_color")
                e.description = """
                    **You have permission to access the following commands:**
                    #{(OIDs.include?(event.author.id)) ? 'syscall' : ''}
                    #{(OIDs.include? event.author.id) ? 'updatebot' : ''} 
                    #{(OIDs.include? event.author.id) ? 'viewlog' : ''} 
                    modconf {key} {value}
                """
            end
        else 
            cmd.downcase!
            case cmd
                when "syscall"; event.respond "#{(OIDs.include? event.author.id) ? 'syscall | runs a command on the system hosting the bot(s).' : 'You do not have permission to access this command!'}"; 
                # when cmd == "prune" || cmd == "purge"; event.respond "#{(event.author.permission? :manage_messages) ? 'prune/purge {amount} | removes {amount} messages sent within the last 14 days. Max: 99' : 'You do not have permission to access this command!'}"
                # when cmd == "pruneall" || cmd == "purgeall"; event.respond event.respond "#{(event.author.permission? :manage_messages) ? 'pruneall/purgeall | removes the last 700 messages sent within the last 14 days. You may run multiple times.' : 'You do not have permission to access this command!'}"
                when "updatebot"; event.respond "#{(OIDs.include? event.author.id) ? 'updatebot | update the bot(s) to the latest GitLab commit on branch master' : 'You do not have permission to access this command!'}."; 
                when "modconf"; event.respond "modconf {key} {value} | modify per-user configuration. Available keys: embed_color (value: hex color without hashtag)"
                when "viewlog"; event.respond "#{(OIDs.include? event.author.id) ? 'viewlog | view bot logs' : 'You do not have permission to access this command!'}."
            end
        end
    end
end

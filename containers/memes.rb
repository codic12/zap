require 'net/http'
module Meme
    extend Discordrb::Commands::CommandContainer
    command(:meme) do |event|
        resp = Net::HTTP.get(URI 'https://meme-api.herokuapp.com/gimme')
        eval "meme = #{resp}"
        event.send_embed do |e|
            e.color = data_fetch("embed_color")
            e.title = "#{meme[:title]} - r/#{meme[:subreddit]}"
            e.image = Discordrb::Webhooks::EmbedImage.new(url: meme[:url])
        end
    end
end

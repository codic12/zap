require 'discordrb'
require 'confrb'
require 'yaml'
OIDs = YAML.load(File.read '.env')['OIDs']
module OwnerOnly
    extend Discordrb::Commands::CommandContainer 
    command(:updatebot, description: "A command to update Zap. For Proxy & Codic only!") do |event|
        if OIDs.include? event.author.id
            event.respond "Updating bot..."
            exit()
        else
            event.respond "Hey, you. Sorry yo, but my creators (Master Codic and Master Proxy) have forbidden me to interact with strangers and update my core!"
        end
    end


    command(:viewlog, description: "A command to view the bot's log. For Proxy & Codic only!") do |event|
        if OIDs.include? event.author.id 
            event.send_file(File.open("log.txt", "r"), caption: "Log attached below:")
        else
            event.respond "Hey, you. Sorry yo, but my creators (Master Codic and Master Proxy) have forbidden me to interact with strangers and give them access to all my logs."
        end
    end

    command(:syscall, description: "Runs {command} on the system hosting Zap; for Proxy & Codic only!") do |event, *command|
        if OIDs.include? event.author.id 
            command = command.join ' '
            output = %x(#{command}) 
            if output.length <= 2000
                event.respond("```\n#{output}\n```")
                nil
            else
                File.write("output.txt", output)
                event.send_file(File.open("output.txt", "r"), caption: "Output was too long (over 2000 characters). Uploading as a file:")
                File.delete("output.txt")
                nil
            end
        else
            event.respond "Hey, you. Sorry yo, but my creators (Master Codic and Master Proxy) have forbidden me to interact with strangers and give them access to me!"
        end
        nil
    end

    
end
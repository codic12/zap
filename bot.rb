# Require libraries
require 'discordrb'
require 'confrb'
require 'net/http'
require 'yaml'
# Require containers
require_relative File.join('containers', 'owner_commands.rb')
require_relative File.join('containers', 'confstuff.rb')
require_relative File.join('containers', 'moderation.rb')
require_relative File.join("containers", "help.rb")
require_relative File.join("containers", "memes.rb")


# Setting up config stuff
config = YAML.load(File.read('.env'))
ztoken = config['ztoken']
zprefix = config['zprefix']
btoken = config['btoken']
bprefix = config['bprefix']

zap = Discordrb::Commands::CommandBot.new(token: ztoken, prefix: zprefix)
bolt = Discordrb::Commands::CommandBot.new(token: btoken, prefix: bprefix)
$db = Confrb.new("zap")

def data_fetch(event, key) # A function for embeds.
    if key.downcase == "embed_color"
        if File.exist?(File.join '.zap', event.author.id.to_s, 'embed_color.cfg')
            return(File.read(File.join '.zap', event.author.id.to_s, 'embed_color.cfg'))
        else
            return "00ff44"
        end
    end
end

zap.include! OwnerOnly
zap.include! ConfStuff
bolt.include! OwnerOnly
bolt.include! ConfStuff
bolt.include! Moderation # Bolt, you take moderation stuff                                          
zap.include! ZapHelp # Zap's help. Bolt's help is in BoltHelp.
zap.include! Meme

zap.ready do # The status
  zap.game = "Join our support server at https://discord.gg/wtCr7P7"
end

zap.run(background=true)
bolt.run(background=true)
bolt.join
zap.join

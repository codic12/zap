# Contributing
To contribute commands, use a block syntax. Please add a description so the help command can auto-generate help from it. The client for the `CommandBot` is a class instance named `zap`. Use the following to create a new command, noting that the `required_permissions array is optional and depends on your use-case and that the description has to be added for command to be accepted:

```ruby
zap.command(:command_name, description: "The description for your command", required_permissions: [:some_perm, :some_other_perm]) do |event, param, another_param|
    # Code for command. See cheat-sheet!
end
```


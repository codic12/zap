# Zap & Bolt
Two highly modular, configurable Discord bots written in Ruby.

# Setup
To setup your own instance of Zap and Bolt, just rename `example.config.json` to `config.json` and fill all the info out

# Invite links 
Invite links for publicly hosted instances:
bolt: https://discordapp.com/api/oauth2/authorize?client_id=667194667598741536&permissions=8&scope=bot
zap: https://discordapp.com/api/oauth2/authorize?client_id=665026347722342420&permissions=8&scope=bot

# Voice support
Voice (playing youtube videos, etc) will be added when Discord.rb's voice bug is fixed - this stops everyone from having to compile another version of Discord.rb

# Contribute
To contribute, feel free to open issues and make PRs!

# Credits
@IpProxyNeon and @Gsbhasin84 (us) for actually making the bot
The Discord.rb library and all that contributed to it
Discord API server (#ruby-discordrb) for being super helpful in the development of the bot
